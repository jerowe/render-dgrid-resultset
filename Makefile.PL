use 5.010000;
use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    NAME              => 'Render::Dgrid::ResultSet',
    VERSION_FROM      => 'lib/Render/Dgrid/ResultSet.pm', # finds $VERSION
    PREREQ_PM         => {}, # e.g., Module::Name => 1.1
    ($] >= 5.005 ?     ## Add these new keywords supported since 5.005
      (ABSTRACT_FROM  => 'lib/Render/Dgrid/ResultSet.pm', # retrieve abstract from module
       AUTHOR         => 'Jillian Rowe <jillian.e.rowe@gmail.com>') : ()),
);

PREREQ_PM => {
    'Carp',
    'Class::MOP',
    'Data::Dumper',
    'Moose',
    'MooseX::NonMoose',
    'Tie::IxHash',
    'Render::ResultSet',
};

TEST_REQUIRES => {
    'Test::More' => '0.88',
}
