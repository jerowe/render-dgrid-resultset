package Render::Dgrid::ResultSet;

use 5.010000;

require Exporter;
use Moose::Role;
use Carp;
use JSON;
use Data::Dumper;
use Tie::IxHash;

with 'Render::Dgrid';

our @ISA = qw(Exporter);

our $VERSION = '0.01';

#render rows as a hashref for dgrid with rest
sub render_rows_as_hashref{
    my $self = shift;
    my $aref = [];

    $self->dgdata([]);
    if(!$self->resultset){ croak "There is no resultset in Dgrid!";}
    if(!$self->columns){ croak "There is are no columns in Dgrid!";}
    if(!$self->display_columns){ croak "There is no columns in Dgrid!";}

    while (my @vals = $self->resultset->cursor->next) {
        next unless @vals;
        my %href;
        @href{@{$self->columns}} = @vals;
        push(@{$self->dgdata},  \%href);
    }
    $self->resultset->cursor->reset;
}

sub get_column_names{
    my $self = shift;
    my($cols);

    return [$self->columns] if $self->columns;

    croak "No resultset given!" if !$self->resultset;
    my @cols = $self->resultset->result_source->columns or croak "Can't call result_source on resultset passed!";

    $self->columns(\@cols);
    $self->display_columns(\@cols) if !$self->display_columns;
}

#render rows as a json string for direct formatting

sub render_header_as_hashref{
    my $self = shift;
    my %href;
    tie %href, 'Tie::IxHash';

    if(!$self->columns){croak "There are no columns in Dgrid";}

    $self->display_column($self->columns) if !$self->display_columns;
    @href{@{$self->columns}} = @{$self->display_columns};

    $self->dgcolumn({});
    $self->dgcolumn->{columns} = \%href;
}

use namespace::autoclean;
1;

__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Render::ResultSet::Dgrid - Perl extension for blah blah blah

=head1 SYNOPSIS

Apply as a role to Render::ResultSet::Base class:

    package MyApp::Table::Dgrid;

    use Moose;
    extends 'Render::ResultSet::Base';
    with 'Render::ResultSet::Dgrid';

    use namespace::autoclean;

    __PACKAGE__->meta->make_immutable;
    1;

=head1 DESCRIPTION

This is a moose role for Render::ResultSet::Base. It must be passed a resultset, and does not need any other values. The idea and some code was stolen from HTML::FormHandler with many thanks. ;)

There are many, many ways to do this, but I'll isolate too main cases. The first is using REST, and the second is storing the data as a JSON string and displaying with the template toolkit.

=head2 RESTful Method

In your Root controller

    sub dgrid :Path('dgrid/xhr') Args(0) {
        my($self, $c) = @_;

        $c->stash( template => 'dgrid_xhr.tt2' );
    }

In your RESTful controller

    package MyApp::Controller::REST;
    use Moose;
    use namespace::autoclean;
    use Data::Dumper;

    use MyApp::Table::Dgrid;

    BEGIN { extends 'Catalyst::Controller::REST' }

    has 'dgrid_table' => (
        isa => 'MyApp::Table::Dgrid',
        is => 'rw',
        default => sub { MyApp::Table::Dgrid->new },
    );

    sub create{
        my($self, $c) = @_;

        my $rs = $c->model('DB::Book')->search();

        $self->dgrid_table->resultset($rs);

        $self->dgrid_table->display_columns([map {ucfirst} $rs->result_source->columns]);
        $self->dgrid_table->process;
    }

    sub url : Local : ActionClass('REST') { }

    sub url_GET {
        my ( $self, $c) = @_;

        $self->create($c);

        $c->req->header('Content-Type' => 'application/json');
        $c->res->content_type('application/json');

        $self->status_ok(
            $c,
            entity => {
                data => $self->dgrid_table->dgdata,
                columns => $self->dgrid_table->dgcolumn,
            },
        );
    }

OR skip the moose object and set the table in $c->stash

    sub create {
        my($self, $c) = @_;

        my $rs = $c->model('DB::Book')->search();
        my $table = MyApp::Table::Dgrid->new(resultset => $rs);

        $table->display_columns([map {ucfirst} $rs->result_source->columns]);
        $table->process;

        $c->stash(dgrid_table => $table);
    }

    sub url : Local : ActionClass('REST') { }

    sub url_GET {
        my ( $self, $c) = @_;

        $self->create($c);

        $c->req->header('Content-Type' => 'application/json');
        $c->res->content_type('application/json');

        $self->status_ok(
            $c,
            entity => {
                data => $c->stash->{dgrid_table}->{dgdata},
                columns => $c->stash->{dgrid_table}->{dgcolumn},
            },
        );
    }

Your dgrid_xhr.tt2 Template should contain the following

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta charset="utf-8">
        <title>Dgrid and Catalyst</title>

        <link rel="stylesheet" type="text/css" href="[% c.uri_for('/static/dojo/resources/dojo.css') %]">
        <link rel="stylesheet" type="text/css" href="[% c.uri_for('/static/dgrid/css/dgrid.css') %]">
        <script type="text/javascript" src="[% c.uri_for('/static/dojo/dojo.js') %]"></script>

        <script type="text/javascript">

        require( { baseUrl: "[% c.uri_for('/static') %]",
                    packages: [  'dojo', 'dgrid', 'xstyle', 'put-selector',]
            });

        </script>
    <script>

        require(["dojo/on","dgrid/Grid"], 
            function(on, Grid){

                    var xhrargs = { 
                        url: "/rest/url",
                        handleAs: "json",
                        preventCache : false,
                        load: function(json_results){
                            return json_results;
                        },
                        error: function(response, ioArgs) {
                            console.error(response);
                            console.error(response.stack);
                        }
                    };

                    function store () {
                        var deferred = dojo.xhrGet(xhrargs);
                        deferred.then(function(data){
                            rendergrid(data);
                        });
                    }

                    function rendergrid(data){
                        var grid = new Grid(
                            data.columns, 
                            "grid");
                        grid.renderArray(data.data);
                    }

                    store();
                });

        </script>

    </head>
    <body class="claro">
        <div id="grid"></div>
    </body>
    </html>


=head2 Static/Template Toolkit Method

In a controller somewhere

    has 'dgrid_table' => (
        isa => 'MyApp::Table::Dgrid',
        is => 'rw',
        default => sub { MyApp::Table::Dgrid->new },
    );

    sub dgrid_static :Path('static') Args(0) {
        my($self, $c) = @_; 

        $self->create($c);
        $c->stash(no_wrapper => 1); 
        $c->stash( template => 'dgrid_static.tt2' );
    }

    sub create{
        my($self, $c) = @_; 

        my $rs = $c->model('DB::Book')->search();

        $self->dgrid_table->resultset($rs);

        $self->dgrid_table->display_columns([map {ucfirst} $rs->result_source->columns]);
        $self->dgrid_table->process;

        $c->stash(dgrid_table => $self->dgrid_table);
    }

And then in your dgrid_static.tt2 template

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta charset="utf-8">
        <title>Tutorial: Hello dgrid!</title>

        <link rel="stylesheet" type="text/css" href="[% c.uri_for('/static/dojo/resources/dojo.css') %]">
        <link rel="stylesheet" type="text/css" href="[% c.uri_for('/static/dgrid/css/dgrid.css') %]">
        <script type="text/javascript" src="[% c.uri_for('/static/dojo/dojo.js') %]"></script>

        <script type="text/javascript">

        require( { baseUrl: "[% c.uri_for('/static') %]",
                    packages: [  'dojo', 'dgrid', 'xstyle', 'put-selector',]   
            });

        </script>

        <script>
            require(["dgrid/Grid", "dojo/domReady!"],
            function(Grid){

                var data = [% dgrid_table.render_rows_as_string %]; 

                var columns = [% dgrid_table.render_header_as_string %]; 

                var grid = new Grid(
                    columns
                , "grid");
                grid.renderArray(data);
            });
        </script>

    </head>
    <body class="claro">
        <div id="grid"></div>
    </body>
    </html>

=head2 OnDemandGrid with JsonRest for Virtual Scrolling

=head1 Tags

=head2 resultset

Required parameter, isa DBIx::Class::ResultSet. All querying should be done in the controller to give the renderer the exact resultset required.

=head2 process

Sets up the dgrid table data and columns from the resultset

=head2 dgcolumn

Column ref portion of dgrid
Perl like this

    my $columns = {columns => {first => "First Name", last => "Last Name", age => "Age"}};

Json like this

    columns: {
        first: "First Name",
        last: "Last Name",
        age: "Age"
    }

=head2 dgdata

Data ref portion of dgrid
Perl hash like this

    my $data = [ {first => "Bob", last => "Barker", age => 89}, {first => "Vanna", last => "White", age => 55}, {first => "Pat", last => "Sajak", age => 65} ];

Json hash like this

    var data = [ 
        { first: "Bob", last: "Barker", age: 89 },
        { first: "Vanna", last: "White", age: 55 },
        { first: "Pat", last: "Sajak", age: 65 }
    ];

=head2 pretty_json

Boolean value to print pretty json or regular json

=head1 SEE ALSO

Template::Toolkit
DBIx::Class
HTML::FormHandler
Render::ResultSet::HTMLTable

=head1 AUTHOR

Jillian Rowe, E<lt>jillian.e.rowe@gmail.com<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Jillian Rowe

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
